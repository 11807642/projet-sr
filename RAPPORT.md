__________                                     __        
\______   \_____  ______ ______   ____________/  |_   /\ 
 |       _/\__  \ \____ \\____ \ /  _ \_  __ \   __\  \/ 
 |    |   \ / __ \|  |_> >  |_> >  <_> )  | \/|  |    /\ 
 |____|_  /(____  /   __/|   __/ \____/|__|   |__|    \/ 
        \/      \/|__|   |__|  

KRIVOKUCA Ivan SI ALI Aghiles

L'objectif de notre projet est un jeu qui fera voyager le joueur à travers un personnage et décidera de ses choix.
Selon ses choix, le personnage aura différentes aventures. Écrit par deux scénaristes géniaux. L'histoire sera fun et divertissante (normalement).

Le principe du code est un échange serveur/client de caractères (textes), on le fait en TCP car ce sont des échanges donc le serveur attend une réponse du client (et vice versa).
Les fichiers de textes sont présents dans le dossier commun.
Lorsque que le client se connectera au serveur, il indiquera son nom d'utilisateur et décide s'il veut jouer. 
Alors le client recevra par le serveur un texte avec des choix a faire (soit d'entrer A,B ou C), et selon les choix que le client choisira, le serveur recevra la réponse et lui enverra la suite du texte correspondant au choix sélectionné (grâce a l'ouverture du fichier correspondant a son choix).
Un maximum de 10 clients est accepté si ce chiffre est dépassé alors le serveur ne les prend pas en compte, néanmoins on peut augmenter cette valeur si on le souhaite.
Tant que le client ne reçoit pas la chaîne "FIN" celui-ci continue de jouer (car recevoir FIN met fin au jeu)
Le serveur possède un gracefull shutdown, c'est a dire que si celui-ci recoit le signal CTRL-c alors le client recevra un message lui dissant que sa partie sera sauvegardé et il pourra reprendre la suite après. Si le client se connecte au même nom d'utilisateur pourra reprendre sa sauvegarde (si un fichier avec son nom a bien été crée)
Le client lorsque il est dans le jeu, peut décider de quitter en entrant écrivant Z, sa partie sera sauvegardé.






